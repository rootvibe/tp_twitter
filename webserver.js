var connect     = require('connect');
var serveStatic = require('serve-static');
var http        = require('http');
var app         = connect();

app.use(serveStatic('www/'));

http.createServer(app).listen(3000);

console.log('Twitter is running on 127.0.0.1:3000 !');
