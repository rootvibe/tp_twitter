function UserAPI(server, redis) {

  server.get({
    url: "/api/v1/user/:user_id/followers"
  }, function(req, res, next) {
    if (!req.params.user_id) {
      return res.send({
        'error': 'No id'
      })
    }

    redis.smembers(req.params.user_id + ':followers', function(err, reply) {
      if (err) {
        return res.send({
          'error': err
        })
      }

      res.send({
        'followers': reply
      });
    })
  })

  server.get({
    url: "/api/v1/user/:user_id/following"
  }, function(req, res, next) {
    if (!req.params.user_id) {
      return res.send({
        'error': 'No id'
      })
    }

    redis.smembers(req.params.user_id + ':following', function(err, reply) {
      if (err) {
        return res.send({
          'error': err
        })
      }

      res.send({
        'following': reply
      });
    })
  })

  server.get({
    url: "/api/v1/user/:user_id/stats"
  }, function(req, res, next) {
    if (!req.params.user_id) {
      return res.send({
        'error': 'No id'
      })
    }

    redis.multi()
      .scard(req.params.user_id + ':followers')
      .scard(req.params.user_id + ':following')
      .llen(req.params.user_id + ':tweets')
      .exec(function(err, replies) {
        res.send({
          "followers": replies[0],
          "following": replies[1],
          "tweets"   : replies[2]
        })
      })
  })
}

module.exports = UserAPI;