var redis   = require('redis');
var options = {
  'host': 'pub-redis-17196.us-east-1-3.2.ec2.garantiadata.com',
  'port': 17196,
  'url' : 'redis://bruel.nicolas85@gmail.com:epsi2016@pub-redis-17196.us-east-1-3.2.ec2.garantiadata.com:17196'
}
var redisClient = redis.createClient(options);

module.exports = {
  bind : function(server) {
    var modules = ['user', 'tweet'];
    modules.map(function(module) {
      require('./' + module)(server, redisClient);
    });
  }
};
