var twitter = require('twitter-text');
var async   = require('async');

function tweetAPI(server, redis) {

  server.post({
    url: "/api/v1/tweets/create"
  }, function(req, res, next) {

    if (req.body.user_id && req.body.text) {
      var timestamp = Date.now();
      var hashtags = twitter.extractHashtags(req.body.text);
      redis.rpush(req.body.user_id + ':tweets', JSON.stringify({
        timestamp: timestamp,
        message: req.body.text,
        user: req.body.user_id
      }), function(err, reply) {
        if (err) {
          return res.send(err);
        }

        var multi = redis.multi()

        multi.zadd(req.body.user_id + ':timeline', timestamp, reply - 1);

        for (var i = 0; i < hashtags.length; ++i) {
          multi.rpush(hashtags[i].toLowerCase() + ':hashtag', req.body.user_id + ':tweets ' + (reply - 1));
        }
        multi.exec(function(err, replies) {
          if (err) {
            return res.send(err);
          }
          res.send('Successfully tweeted, GJ !')
        })
      });
    } else {
      return res.send({
        error: 'User_id undefined'
      })
    }
  });

  server.get({
    url: "/api/v1/tweets/findByHashtag"
  }, function(req, res, next) {
    if (!req.params.hashtag) {
      return res.send({
        error: 'hashtag undefined'
      })
    }

    redis.lrange(req.params.hashtag + ':hashtag', 0, -1, function(err, replies) {
      var tweets = [];
      var multi  = redis.multi();
      if (replies) {
        for (var i = 0; i < replies.length; ++i) {
          multi.lindex(replies[i].split(' '))
        }

        multi.exec(function(err, replies) {
          var parsedTweets = [];
          for (var i = 0; i < replies.length; ++i) {
            parsedTweets.push(JSON.parse(replies[i]));
          }
          res.send(parsedTweets);
        });
      } else {
        res.status(418);
        res.send('no tweets');
      }
    });
  });

  server.get({
    url: "/api/v1/tweets/:user_id"
  }, function(req, res, next) {
    if (!req.params.user_id) {
      return res.send({
        error: 'User_id undefined'
      })
    }
    redis.zrevrange(req.params.user_id + ':timeline', 0, -1, function(err, replies) {
      if (err) {
        return res.send(err);
      }

      var multi = redis.multi();
      for (var i = 0; i < replies.length; ++i) {
        multi.lindex(req.params.user_id + ':tweets', replies[i])
      }

      multi.exec(function(err, replies) {
        if (err) {
          return res.send(err);
        }
        var parsedTweets = [];
        for (var i = 0; i < replies.length; ++i) {
          parsedTweets.push(JSON.parse(replies[i]));
        }
        res.send(parsedTweets);
      });
    });
  })

  server.get({
    url: "/api/v1/tweets/:user_id/timeline"
  }, function(req, res, next) {
    if (!req.params.user_id) {
      return res.send({
        error: 'User_id undefined'
      })
    }

    var tweets = []

    redis.smembers(req.params.user_id + ':following', function(err, replies) {

      if (err) {
        return res.send({
          'error': err
        })
      }

      var multi = redis.multi();
      var users = replies;
      users.push(req.params.user_id);
      for (var i = 0; i < replies.length; ++i) {
        multi.lrange(replies[i] + ':tweets', 0, -1)
      }

      multi.exec(function(err, replies) {
        for (var i = 0; i < replies.length; ++i) {
          tweets = tweets.concat(replies[i]);
        }
        var parsedTweets = [];
        for (var i = 0; i < tweets.length; ++i) {
          parsedTweets.push(JSON.parse(tweets[i]))
        }

        async.sortBy(parsedTweets, function(tweet, f) {
          f(null, tweet.timestamp);
        }, function(err, results) {
          return res.send(results.reverse());
        })
      })
    });
  });
}

module.exports = tweetAPI;
