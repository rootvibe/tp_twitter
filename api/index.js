var restify  = require('restify');
var _        = require('lodash');
var routes   = require('./routes');
var server   = restify.createServer();

server.startServer = function(port, f) {
  server.use(restify.CORS({origins:[
    'http://localhost:3000',
    'http://127.0.0.1:3000'
  ]}));

  server.use(restify.acceptParser(server.acceptable));
  server.use(restify.authorizationParser());
  server.use(restify.dateParser());
  server.use(restify.queryParser());
  server.use(restify.bodyParser());
  server.use(restify.gzipResponse());
  server.use(restify.fullResponse());

  routes.bind(server);

  console.info("Loading routes");
  _.forEach(server.router.routes, function(routes, method){
    _.forEach(routes, function(route){
      console.info('-> ' + route.method + ' ' + route.spec.url + ' ' + (route.spec.label || ''));
    });
  });
  return server.listen(port, f);
}

module.exports = server;
