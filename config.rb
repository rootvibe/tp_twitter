require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
css_dir  = "build/css/"
sass_dir = "www/ressources/scss/"

output_style = :compressed
