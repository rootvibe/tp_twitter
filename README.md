# README #

Install node modules
```bash
npm install
```

Compile ressources
```bash
gulp
```

Start the API
```bash
node apiserver.js
```

Start the web server
```bash
node webserver.js
```

Then go to http://localhost:3000/ and in the right top corner, select a user