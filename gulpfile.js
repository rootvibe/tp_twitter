var gulp       = require('gulp');
var concat     = require('gulp-concat');
var uglify     = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var nodemon    = require('gulp-nodemon');
var sass       = require('gulp-sass');

// Libs paths (for order of include)
var libs = [
  'www/shared/javascripts/angular.min.js',
  'www/shared/javascripts/angular-ui-router.js',
  'www/shared/javascripts/angular-cookies.js',
  'www/shared/javascripts/jquery.js',
  'www/shared/javascripts/bootstrap.js'
];

// Paths for app.js and router.js
var app_router = [
  'www/shared/app.js',
  'www/shared/router.js'
];

// Paths initialization
var paths = {
  build_root : 'www/build/',
  controllers: 'www/shared/controllers/*.js',
  templates  : 'www/shared/templates/*.html',
  style_libs : 'www/shared/css/*',
  fonts      : 'www/shared/fonts/*',
  services   : 'www/shared/services/*.js',
  images     : 'www/ressources/img/*',
  sass_inc   : 'www/ressources/scss/partials/*',
  stylesheets: 'www/ressources/scss/*.scss',
  app        : app_router,
  libs       : libs
};

// Watcher for API Server
gulp.task('start', function () {
  nodemon({
    script: 'apiserver.js',
    ignore: '/www/*',
    ext: '*.js',
    env: { 'NODE_ENV': 'development' }
  })
});

// Compile app/router
gulp.task('app', [], function() {
  return gulp.src(paths.app)
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest(paths.build_root));
});

// Compile scss
gulp.task('sass', function() {
  return gulp.src(paths.stylesheets)
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({includePaths: [paths.sass_inc]}))
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest(paths.build_root + 'css/'));
});

// Compile controllers
gulp.task('controllers', [], function() {
  return gulp.src(paths.controllers)
    .pipe(uglify())
    .pipe(concat('controllers.min.js'))
    .pipe(gulp.dest(paths.build_root));
});

// Compile services
gulp.task('services', [], function() {
  return gulp.src(paths.services)
    .pipe(uglify())
    .pipe(concat('services.min.js'))
    .pipe(gulp.dest(paths.build_root));
});

// Compile templates
gulp.task('templates', [], function() {
  return gulp.src(paths.templates)
    .pipe(gulp.dest(paths.build_root + 'templates/'));
});

// Compile css librairies
gulp.task('css_libs', [], function() {
  return gulp.src(paths.style_libs)
    .pipe(gulp.dest(paths.build_root + 'css/libs/'));
});

// Compile fonts
gulp.task('fonts', [], function() {
  return gulp.src(paths.fonts)
    .pipe(gulp.dest(paths.build_root + 'css/fonts/'));
});

// Compile images
gulp.task('images', [], function() {
  return gulp.src(paths.images)
    .pipe(gulp.dest(paths.build_root + 'img/'));
});

// Compile javascripts
gulp.task('js', [], function() {
  return gulp.src('www/ressources/javascripts/*.js')
    .pipe(uglify())
    .pipe(concat('scripts.min.js'))
    .pipe(gulp.dest(paths.build_root + 'javascript'));
});

// Compile librairies
gulp.task('js_libs', [], function() {
  return gulp.src(paths.libs)
    .pipe(uglify())
    .pipe(concat('libs.min.js'))
    .pipe(gulp.dest(paths.build_root + 'javascript'));
});

// Watcher for app/router
var watcher = gulp.watch(paths.app, ['app']);
watcher.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for controllers
var controllers = gulp.watch(paths.controllers, ['controllers']);
controllers.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for services
var services = gulp.watch(paths.services, ['services']);
services.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for images
var images = gulp.watch(paths.images, ['images']);
images.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for stylesheets
var styles = gulp.watch(paths.stylesheets, ['sass']);
styles.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for templates
var templates = gulp.watch(paths.templates, ['templates']);
templates.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for javasripts
var js = gulp.watch('www/ressources/javascripts/*.js', ['js']);
js.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for css librairies
var css_libs = gulp.watch(paths.style_libs, ['css_libs']);
css_libs.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for fonts
var fonts = gulp.watch(paths.style_libs, ['fonts']);
fonts.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Watcher for librairies
var js_libs = gulp.watch(paths.libs, ['js_libs']);
js_libs.on('change', function(event) {
  var path = event.path.split('/');
  console.log('File ' + path[path.length - 1] + ' was ' + event.type + ', running tasks...');
});

// Default task for Gulp
gulp.task('default', [
  'app', 'controllers', 'sass', 'css_libs', 'services',
  'templates', 'images', 'js_libs', 'fonts', 'js'
]);
