darkbird.config(function($stateProvider, $urlRouterProvider, $locationProvider) {

  $urlRouterProvider.otherwise('/404');

  $stateProvider
    .state('home', {
      url        : '/',
      templateUrl: '/build/templates/home.html',
      controller : 'HomeController as home'
    })
    .state('followers', {
      url        : '/followers/',
      templateUrl: '/build/templates/followers.html',
      controller : 'FollowersController as followers'
    })
    .state('followings', {
      url        : '/followings/',
      templateUrl: '/build/templates/following.html',
      controller : 'FollowingController as following'
    })
    .state('stats', {
      url        : '/stats/',
      templateUrl: '/build/templates/stats.html',
      controller : 'StatsController as stats'
    })
    .state('404', {
      url        : '/404/',
      templateUrl: '/build/templates/404.html'
    });

});
