darkbird.controller('FollowingController', ['$scope', '$http', '$stateParams', '$cookies', 'Config',
 function($scope, $http, $stateParams, $cookies, Config) {

   var controller = this;

   controller.getData = function (user) {
     $http({
       method: 'GET',
       url   : Config.ROOT_URL + Config.API_URL + 'user/' + user + '/following'
     }).then(function onSuccess(response) {
       controller.following = response.data.following;
     }, function onError(response) {
       controller.following = ['results cant be processed :('];
     });
   }

   controller.getData($cookies.get('user'));
   $scope.$watch(function() {
     return $cookies.get('user');
   }, controller.getData);

}]);
