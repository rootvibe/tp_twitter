darkbird.controller('FollowersController', ['$scope', '$http', 'Config', '$cookies',
 function($scope, $http, Config, $cookies) {

  var controller = this;

  controller.getData = function (user) {
    $http({
      method: 'GET',
      url   : Config.ROOT_URL + Config.API_URL + 'user/' + user + '/followers'
    }).then(function onSuccess(response) {
      controller.followers = response.data.followers;
    }, function onError(response) {
      controller.followers = ['results cant be processed :('];
    });
  }

  controller.getData($cookies.get('user'));
  $scope.$watch(function() {
    return $cookies.get('user');
  }, controller.getData);

}]);
