darkbird.controller('StatsController', ['$scope', '$http', '$stateParams', '$cookies', 'Config',
 function($scope, $http, $stateParams, $cookies, Config) {

   var controller  = this;

   controller.getData = function (user) {
     $http({
       method: 'GET',
       url   : Config.ROOT_URL + Config.API_URL + 'user/' + user + '/stats'
     }).then(function onSuccess(response) {
       controller.stats = response.data;
     }, function onError(response) {
       controller.stats = {
         followers: 'n/a',
         following: 'n/a',
         tweets   : 'n/a'
       };
     });

     $http({
       method: 'GET',
       url   : Config.ROOT_URL + Config.API_URL + 'tweets/' + user
     }).then(function onSuccess(response) {
       controller.tweets = response.data;
     }, function onError(response) {
       controller.tweets = ['results cant be processed :('];
     });
   };

   controller.getData($cookies.get('user'));
   $scope.$watch(function() {
     return $cookies.get('user');
   }, controller.getData);

}]);
