darkbird.controller('HomeController', ['$scope', '$rootScope', '$http', '$cookies', '$state', 'Config',
 function($scope, $rootScope, $http, $cookies, $state, Config) {

  if ($cookies.get('user') == undefined) {
    $cookies.put('user', 'nicolas');
  }

  var controller  = this;
  controller.user = $cookies.get('user');

  controller.getData = function (user) {
    $http({
      method: 'GET',
      url   : Config.ROOT_URL + Config.API_URL + 'tweets/' + user + '/timeline'
    }).then(function onSuccess(response) {
      $rootScope.tweets = response.data;
    }, function onError(response) {
      $rootScope.tweets = ['results cant be processed :('];
    });
    $rootScope.state = 'timeline';
  };

  controller.searchHashtag = function($event) {
    if ($event.keyCode != 13) return;
    if (controller.hashtag === '' || controller.hashtag == undefined) return;

    $http({
      method: 'GET',
      url   : Config.ROOT_URL + Config.API_URL + 'tweets/findByHashtag?hashtag='
        + controller.hashtag
    }).then(function onSuccess(response) {
      $rootScope.tweets  = response.data;
      $rootScope.hashtag = controller.hashtag;
    }, function onError(response) {
      $rootScope.tweets = ['results cant be processed :('];
    });
    $rootScope.state = 'hashtag';
  };

  controller.publish = function () {
    if (controller.message === '' || controller.message == undefined) return;

    $http({
      method: 'POST',
      url   : Config.ROOT_URL + Config.API_URL + 'tweets/create',
      data  : {
        "user_id": $cookies.get('user'),
        "text"   : controller.message
      }
    }).then(function onSuccess(response) {
      controller.getData(controller.user);
      controller.message = undefined;
    }, function onError(response) {
      console.log(repsonse);
    });
  };

  controller.changeuser = function(user) {
    $cookies.put('user', user);
    controller.user = $cookies.get('user');
    $state.go($state.current.name);
  };

  controller.getData($cookies.get('user'));

  $scope.$watch(function() {
    return $cookies.get('user');
  }, controller.getData);

}]);
