darkbird.service('Config', function() {

  var config = {
    API_URL  : 'api/v1/',
    ROOT_URL : 'http://127.0.0.1:3001/'
  };

  return config;
});
